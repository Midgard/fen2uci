# fen2uci

(Pronounce like Italian *fentucci*)

A wrapper that makes primitive chess bots that work with FEN boards play nice with UCI supporting GUIs.

You can play the bot using such programs as [PyChess](http://www.pychess.org/) and [Knights](https://www.kde.org/applications/games/knights/).

## Installation

All tools should be the Python 3 versions. Depending on your distribution, you might have to use `pip3` instead of `pip`.

Run as root (convenient system-wide installation) or in a virtualenv:
```
pip install git+https://framagit.org/Midgard/fen2uci.git#egg=fen2uci
```

To upgrade if there's a new version available, add the `--upgrade` flag:
```
pip install --upgrade git+https://framagit.org/Midgard/fen2uci.git#egg=fen2uci
```

## Usage
*Note: if you installed in a virtualenv, use `/path/to/the/env/bin/fen2uci` instead of `fen2uci` or `/usr/bin/fen2uci`.*

Add an UCI engine in your chess program:
* executable/command: `fen2uci`
* arguments: `--name <name for bot> /path/to/fen_chess_bot`
	* `<name for bot>` is the name you would like your chess bot to identify itself as.
	* If your program doesn't have a separate "arguments" field, append this text to the command field.
* protocol: UCI

There's a bug in PyChess that may prevent you from adding the chess engine. Try adding this to the list in PyChess's `engines.json`* file:
```json
 {
  "name": "<name for bot>",
  "command": "/usr/bin/fen2uci",
  "args": ["--name", "<name for bot>", "/path/to/fen_chess_bot"],
  "country": "<2-letter code for country>",
  "protocol": "uci",
  "recheck": false,
  "variants": [
   "normal",
   "fischerandom"
  ]
 }
```

\* On Linux, this is probably in `~/.config/pychess/engines.json`.
