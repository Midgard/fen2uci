#!/usr/bin/env python3

# This file is part of fen2uci.
#
# fen2uci is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fen2uci is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fen2uci.  If not, see <http://www.gnu.org/licenses/>.

import sys, os
import traceback
import argparse
import time
import subprocess
import chess


class Bot():
    def __init__(self, name, executable):
        self.name       = name
        self.executable = executable


    def run(self, args, timeout=600):
        return subprocess.run((self.executable, *args), timeout=timeout, check=True,
                              stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


    def next_board(self, fen: str):
        p = self.run(fen.split(" "))
        return (p.stdout.decode("utf-8").strip(), p.stderr.decode("utf-8").strip())


    def __repr__(self):
        return "bot.Bot(name={0.name!r}, executable={0.executable!r})".format(self)


class UCIWrapper:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.board = chess.Board()
        self.debug = False
        self.debugfile = None


    def send(self, msg):
        self.debug_log("sending: " + msg)

        print(msg, flush=True)


    def msg(self, msg):
        self.send("info string {}".format(msg))


    def debug_msg(self, msg):
        if self.debug:
            self.msg(msg)


    def debug_log(self, msg):
        if self.debug:
            if self.debugfile is None:
                self.debugfile = open("uciwrapper.log", "w")
                self.msg("created new log file at {}".format(os.getcwd()))
            print(msg, file=self.debugfile, flush=True)


    def cmd(self, c: str):
        self.debug_log("received command {}".format(c))

        c = c.split(" ")
        try:
            f = getattr(self, "_" + c[0])
        except AttributeError:
            self.msg("command is not valid UCI or is not implemented: {}".format(" ".join(c)))
            return

        try:
            return f(*c[1:])
            sys.exit()
        except NotImplementedError as e:
            self.msg("info string not implemented: {}".format(e))
        except BaseException as e:
            self.msg("error: {}".format(e))


    def _uci(self):
        self.send("id name " + self.bot.name)
        self.send("uciok")


    def _ucinewgame(self):
        self.board.reset()


    def _isready(self, *_):
        self.send("readyok")


    def _register(self, *_):
        self.send("registration ok")


    def _setoption(self, *args):
        self.msg("ignoring option '{}'".format(" ".join(args)))


    def _debug(self, on):
        if on == "on":
            self.debug = True
            self.msg("debugging enabled")
        elif on == "off":
            self.debug = False
            if self.debugfile is not None:
                self.debugfile.close()
                self.debugfile = None

            self.msg("debugging disabled")
        else:
            raise ValueError("argument to debug must be either on or off")


    def _position(self, *args):
        # First arg may be "startpos"
        if args[0] == "startpos":
            self.board.reset()
            args = args[1:]

        # First args may be a FEN board
        elif args[0] == "fen":
            self.board.set_fen(" ".join(args[1:7]))
            args = args[7:]

        if not args:
            return

        if args[0] != "moves":
            raise ValueError("expected keyword 'moves' after setup but got " + args[0])
        args = args[1:]

        for uci_move in args:
            self.board.push_uci(uci_move)


        if self.debug:
            self.msg("setup board: " + self.board.fen())


    def _go(self, *_):
        self.debug_msg("running engine")

        start = time.perf_counter()
        next_board_str, stderr = self.bot.next_board(self.board.fen())
        duration = time.perf_counter() - start

        if stderr:
            self.debug_log("engine sent on stderr: ```\n{}\n```".format(stderr))

        if self.debug:
            self.debug_msg("engine took {:.1f} seconds to answer".format(duration))
            self.debug_msg("engine answered: {!r}".format(next_board_str))

        if next_board_str == "DRAW":
            self.send("bestmove 0000")
            return

        next_board = chess.Board(next_board_str)
        if not next_board.is_valid():
            self.msg("error: underlying engine didn't return valid FEN board")
            sys.exit()

        move = detect_one_move(self.board, next_board.fen())

        if not move:
            self.msg("error: underlying engine didn't return logical successor")
            sys.exit()

        self.send("bestmove " + move.uci())


    def _stop(self):
        # lol
        pass


    def _quit(self):
        sys.exit()


    def _ponderhit(self, *_):
        raise NotImplementedError("the UCI wrapper does not support pondering")


def detect_one_move(board1: chess.BaseBoard, board2fen: str):
    """
    board2 can be the FEN representation or a chess.BaseBoard
    """

    # Try each possible move to see which one yields board2
    for move in board1.pseudo_legal_moves:
        board1.push(move)
        if board1.fen() == board2fen:
            board1.pop()
            return move
        board1.pop()
    return None


def main():
    parser = argparse.ArgumentParser(description='Wrap a useless FEN bot in a UCI interface')
    parser.add_argument('executable', help='path to executable')
    parser.add_argument('--name', help='name under which the engine should identify itself. defaults to executable name', default=None)
    args = parser.parse_args()

    name = args.executable if args.name is None else args.name
    bot = Bot(name, args.executable)
    wrapper = UCIWrapper(bot)

    while True:
        wrapper.cmd(input())


if __name__ == "__main__":
    main()

