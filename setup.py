from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='fen2uci',
      version='1.3',
      description='UCI wrapper for primitive chess bots that work with FEN boards',
      classifiers=[
        'Environment :: Console',
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
      ],
      url='https://framagit.org/Midgard/fen2uci',
      author='Midgard',
      license='GPLv3+',
      packages=['fen2uci'],
      install_requires=[
          'python-chess',
      ],
      entry_points = {
          'console_scripts': ['fen2uci=fen2uci:main'],
      },
      zip_safe=False)
