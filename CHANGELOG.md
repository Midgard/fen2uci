# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).
This project does not adhere to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3] - 2018-06-19
### Added
- Time the engine response in debug mode.
- Print the engine's stderr to the log file in debug mode.

### Changed
- Increase timeout from 40 seconds to 10 minutes.

## [1.2] - 2018-06-17
### Fixed
- Fix a bug where the wrapper would not support setting `position fen …`, necessary for Chess960 and arbitrary analyses.

### Changed
- Increase timeout from 40 seconds to 10 minutes.

## [1.1] - 2018-06-16
### Fixed
- Fix a bug where the wrapper would not support setting `position startpos` on its own.

## [1.0] - 2018-06-16
- Initial release.
